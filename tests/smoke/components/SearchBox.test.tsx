import { shallow } from "enzyme";
import React from "react";

import { SearchBox } from "../../../src/components/SearchBox";

it("renders without crashing", () => {
  const noop = () => undefined;

  expect(() => shallow(<SearchBox onChange={noop} />)).not.toThrowError();
});
